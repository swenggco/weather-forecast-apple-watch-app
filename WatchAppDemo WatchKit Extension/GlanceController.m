//
//  GlanceController.m
//  WatchAppDemo WatchKit Extension
//
//  Created by Aamer Khalid on 10/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import "GlanceController.h"
#define kAPPGROUPIDENTIFIER @"group.com.swenggco.weatherapp"


@interface GlanceController()
@property (weak, nonatomic) IBOutlet WKInterfaceImage *weatherStatusImageView;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *weatherDiscriptionLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *locationLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *highTemeratureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lowTemeratureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *mainGroup;
@end


@implementation GlanceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    [self.mainGroup setBackgroundImage:[UIImage imageNamed:@"BGwatch"]];
    
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    
    NSURL *directoryURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAPPGROUPIDENTIFIER];
    NSDictionary *weatherDictionary=[CDPlistManager readFromFile:@"weather" filePath:[directoryURL path]];
    
    [self.locationLabel setText:weatherDictionary[@"location"]];
    
    
    [self.weatherDiscriptionLabel setText:weatherDictionary[@"temperatureCondiion"]];
    
    [self.temperatureLabel setText:weatherDictionary[@"temperature"]];
    
    Climacon climacon = [WeatherConditionIcons climaconCharacterForDescription:weatherDictionary[@"temperatureCondiion"]];
    NSString *iconName=[WeatherConditionIcons getWeatherConditionIcon:climacon];
    
    [self.weatherStatusImageView setImage:[UIImage imageNamed:iconName]];

    
    [self.highTemeratureLabel setText:[NSString stringWithFormat:@"H:%@",weatherDictionary[@"High"]]];

     [self.lowTemeratureLabel setText:[NSString stringWithFormat:@"L:%@",weatherDictionary[@"Low"]]];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



