//
//  main.m
//  WatchAppDemo
//
//  Created by Aamer Khalid on 10/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
