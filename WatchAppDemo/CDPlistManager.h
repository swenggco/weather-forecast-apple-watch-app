//
//  CDPlistManager.h
//
//  Created by Aamer Khalid on 21/10/2014.
//  Copyright (c) 2014 Swenggco Software. All rights reserved.
//

//  class for working with files type .plist 

#import <Foundation/Foundation.h>

@interface CDPlistManager : NSObject

{
    
}

// create file
+(void)createFile:(NSString *)fileName;
+(void)createFile:(NSString *)fileName filePath:(NSString*)path;
+(void)createFile:(NSString *)fileName dataToFile:(NSMutableDictionary*)data;
+(void)createFile:(NSString *)fileName filePath:(NSString*)path 
                                dataToFile:(NSMutableDictionary *)data;


// write to file
+(void)writeToFile:(NSString*)fileName dataToFile:(NSMutableDictionary*)data;
+(void)writeToFile:(NSString*)fileName dataToFile:(NSMutableDictionary*)data 
                                filePath:(NSString*)path;

// read from file
+(NSDictionary*)readFromFile:(NSString*)fileName;
+(NSDictionary*)readFromFile:(NSString*)fileName filePath:(NSString*)path;
+(NSMutableArray*)readFromResourcePlist:(NSString*)namePlist;

// output file names are on the way "./document/path"
+(NSArray*)checkFileFromFolder:(NSString*)path;

/*!
 * @discussion Delete Record from Plist
 * @param filePath
 * @return nil
 */
+(void)deleteFile:(NSString*)filePath;


/*!
 * @discussion Getting Value From Plist from Key Value
 * @param plistName
 * @param key
 * @return NSdictionary
 */
+ (id)valuePlist:(NSString *)plistName withKey:(NSString*)key;


/*!
 * @discussion update the plist values with respect to key
 * @param plistName
 * @param key
 * @param string
 * @return nil
 */
+(void)editStringPlist:(NSString *)plistName withKey:(NSString*)key andString:(id)string;
@end
