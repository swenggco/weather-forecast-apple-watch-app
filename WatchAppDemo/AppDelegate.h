//
//  AppDelegate.h
//  WatchAppDemo
//
//  Created by Aamer Khalid on 10/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

